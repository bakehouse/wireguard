######### 
WireGuard
#########

| Building WireGuard server images for K8S integration, 
| i'm not using persistent configuration for server side.

*******
Install
*******

| see https://www.wireguard.com/install/
|
| If you just want to try wireguard beetween docker containers, run alpine instances:

.. code-block:: bash

   docker run --rm --privileged=true -it alpine:latest
   apk add -U wireguard-tools

*******************
Peer Identification
*******************

| Clients and server both have private/public key pair.
| Public key is derived from private key.
| Have a try, and repeat this step on each node

.. code-block:: bash

   K=$(wg genkey); echo -e "PRIVATE KEY : $K \nPUBLIC KEY : $(echo $K|wg pubkey)"

   # Output example:
   # PRIVATE KEY : mOUFuuS+gJP6vqGwwNu95LDmOHAO+uFwdM3NjChiU1c=
   # PUBLIC KEY : ZLgnlkL9xrpmri7zxzX16iy6gm0Mvwx9QP9M3hmp5gc=

***********
Server Side
***********

| Let's first run a server instance without clients.

.. code-block:: bash

   # Replace private key with a new one
   PRIVATE_KEY='mOUFuuS+gJP6vqGwwNu95LDmOHAO+uFwdM3NjChiU1c='
   umask 077; echo "$PRIVATE_KEY" >/etc/wireguard/privatekey

   echo -n 1 >/proc/sys/net/ipv4/ip_forward
   ip link add dev wg0 type wireguard
   wg set wg0 listen-port 3000 private-key /etc/wireguard/privatekey
   ip address add dev wg0 10.0.0.1/24
   ip link set up dev wg0
   iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

   # Try 'wg' command to get wireguard status:
   # $ wg
   # interface: wg0
   #   public key: ZLgnlkL9xrpmri7zxzX16iy6gm0Mvwx9QP9M3hmp5gc=
   #   private key: (hidden)
   #   listening port: 3000

| Server is listening on port 3000,
| We are defining 10.0.0.0/24 as peers network, and we take the 1rst IP. 
| MASQUERADE transform clients udp traffic to match host IP source when going outside

***********
Client Side
***********

| Create 1rst client configuration file and run it,
| Client ip ``10.0.0.3/24``, access ``0.0.0.0/0`` network through ``172.17.0.3:3000`` wireguard server 
| Client private key : ``6IoUjqrzM7xlLIU815nyprLVXVudfze6q9guK+QeKk0=``
| Client public key : ``FJdIuI6BPhunitlqllIhJIGwz1qkA8KD6IWWOSDSnRk=``

.. code-block:: bash

   umask 077;tee /etc/wireguard/wg0.conf <<EOF
   [Interface]   
   PrivateKey = 6IoUjqrzM7xlLIU815nyprLVXVudfze6q9guK+QeKk0=   
   Address = 10.0.0.3/24
   DNS = 1.1.1.1   

   [Peer]   
   AllowedIPs = 0.0.0.0/0   
   PublicKey = ZLgnlkL9xrpmri7zxzX16iy6gm0Mvwx9QP9M3hmp5gc=   
   Endpoint = 172.17.0.2:3000   
   PersistentKeepalive = 25
   EOF

   # Run configuration
   wg-quick up wg0

   # Output example:
   # [#] ip link add wg0 type wireguard
   # [#] wg setconf wg0 /dev/fd/63
   # [#] ip -4 address add 10.0.0.3/24 dev wg0
   # [#] ip link set mtu 1420 up dev wg0
   # [#] resolvconf -a wg0 -m 0 -x
   # [#] wg set wg0 fwmark 51820
   # [#] ip -4 route add 0.0.0.0/0 dev wg0 table 51820
   # [#] ip -4 rule add not fwmark 51820 table 51820
   # [#] ip -4 rule add table main suppress_prefixlength 0
   # [#] sysctl -q net.ipv4.conf.all.src_valid_mark=1
   # [#] iptables-restore -n

| Now we need to declare our client on server side

.. code-block:: bash

   # Server side
   wg set wg0 peer FJdIuI6BPhunitlqllIhJIGwz1qkA8KD6IWWOSDSnRk= allowed-ips 10.0.0.3/32


| Here you go !
| Let's check connexion beetween peers with wg command
| Client Side :

.. code-block::

  interface: wg0
    public key: FJdIuI6BPhunitlqllIhJIGwz1qkA8KD6IWWOSDSnRk=
    private key: (hidden)
    listening port: 48913
    fwmark: 0xca6c
  
  peer: ZLgnlkL9xrpmri7zxzX16iy6gm0Mvwx9QP9M3hmp5gc=
    endpoint: 172.17.0.2:3000
    allowed ips: 0.0.0.0/0
    latest handshake: 1 minute, 29 seconds ago
    transfer: 752 B received, 17.54 KiB sent
    persistent keepalive: every 25 seconds

| Server Side :

.. code-block::

  interface: wg0
    public key: ZLgnlkL9xrpmri7zxzX16iy6gm0Mvwx9QP9M3hmp5gc=
    private key: (hidden)
    listening port: 3000
  
  peer: FJdIuI6BPhunitlqllIhJIGwz1qkA8KD6IWWOSDSnRk=
    endpoint: 172.17.0.3:48913
    allowed ips: 10.0.0.3/32
    latest handshake: 1 minute, 29 seconds ago
    transfer: 1.64 KiB received, 752 B sent

| If you want to stop Wireguard, use command : wg-quick down wg0

.. code-block:: bash

   wg-quick down wg0
   # Output example:
   # [#] ip -4 rule delete table 51820
   # [#] ip -4 rule delete table main suppress_prefixlength 0
   # [#] ip link delete dev wg0
   # [#] resolvconf -d wg0 -f
   # [#] iptables-restore -n



####
Tips
####

| OS with NetworkManager may encounter errors with DNS enforcing, disable it in configuration or install openresolv apt package.
|
| If your OS support systemctl, you can use it to run wg-quick : systemctl start wg-quick@wg0
| To automatically start VPN on boot : systemctl enable wg-quick@wg0

