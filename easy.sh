#!/bin/bash

ENDPOINT="1.2.3.4"
PORT="3000"
INTERFACE="ens2"
DNS="1.1.1.1"
NET="10.0.0"
PRIVATE_KEY=$(wg genkey)
PUBLIC_KEY=$(echo "$PRIVATE_KEY"|wg pubkey)
umask 077; echo "$PRIVATE_KEY" >/etc/wireguard/privatekey

# Replace interface 
cat << EOF > "/root/wgrun.sh"
echo -n 1 >/proc/sys/net/ipv4/ip_forward
ip link add dev wg0 type wireguard
wg set wg0 listen-port 3000 private-key /etc/wireguard/privatekey
ip address add dev wg0 $NET.1/24
ip link set up dev wg0
iptables -t nat -A POSTROUTING -o "$INTERFACE" -j MASQUERADE
for i in \$(wg show wg0 | grep 'peer:' | awk '{print \$2}'); do wg set wg0 peer \$i remove ; done
EOF

for i in {2..9};do 

CLI_PRIVATE_KEY=$(wg genkey)
CLI_PUBLIC_KEY=$(echo "$CLI_PRIVATE_KEY"|wg pubkey)
echo "wg set wg0 peer $CLI_PUBLIC_KEY allowed-ips $NET.$i/32" >> /root/wgrun.sh

cat << EOF > "/root/wg$i.conf"
[Interface]
PrivateKey = $CLI_PRIVATE_KEY
Address = $NET.$i/24
DNS = $DNS

[Peer]
AllowedIPs = 0.0.0.0/0
PublicKey = $PUBLIC_KEY
Endpoint = $ENDPOINT:$PORT
PersistentKeepalive = 25
EOF

done

bash /root/wgrun.sh