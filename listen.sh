#!/bin/bash
#KEY="$1"
KEY="$(cat /root/key)"

set -x
echo -n 1 >/proc/sys/net/ipv4/ip_forward
cd /etc/wireguard; umask 077; echo "$KEY" | tee privatekey | wg pubkey > publickey
ip link add dev wg0 type wireguard
wg set wg0 listen-port 3000 private-key /etc/wireguard/privatekey
ip address add dev wg0 10.0.0.1/24
ip link set up dev wg0
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

set +x
sleep 10

SUMOK="start"
while true;do 
  SUM=$(sha512sum /root/users)
  if [ "$SUMOK" != "$SUM" ];then
    sh -x /root/users && SUMOK=$SUM && echo "LOADED USERS"
  fi
  sleep 20
  echo -e "\n$(date)"
  wg | strings
done
