FROM alpine:latest
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static tini
ADD listen.sh .
RUN \
apk add -U wireguard-tools iptables && \
chmod 500 tini && chmod 700 listen.sh

ENTRYPOINT ["/tini", "--"]
CMD ["/listen.sh"]
